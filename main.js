function countNumbers() {
    let result = 0;
    let regExp = /[- ]/g;
    let a = ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
    let b = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];
    let c = ['twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];
    let d = ['one-hundred', 'two-hundred', 'three-hundred', 'four-hundred', 'five-hundred', 'six-hundred', 'seven-hundred', 'eight-hundred', 'nine-hundred'];
    var e = ['one-thousand'];

    // Count 1 -> 19.
    for (let word of a.concat(b)) {
      // console.info(word);
      result += word.length;
    }
    
    /* Count 20 -> 99. */
    for (let prefixWord of c) {
      // console.info(prefixWord);
      // Join c with a.
      for (let suffixWord of a) {
        let ca = prefixWord + '-' + suffixWord;
        // console.info(ca);
        result += ca.replace(regExp,'').length;
      }
      result += prefixWord.length;
    }

    /* Count 100 -> 999 */
    for (let prefixWord of d) {
      console.info(prefixWord);
      // Join d with a, b, c and ca.
      for (let suffixWordA of a) {
        let da = prefixWord + ' and ' + suffixWordA;
        // console.info(da);
        result += da.replace(regExp,'').length;
      }
      for (let suffixWordB of b) {
        let db = prefixWord + ' and ' + suffixWordB;
        // console.info(db);
        result += db.replace(regExp,'').length;
      }
      for (let suffixWordC of c) {
        let dc = prefixWord + ' and ' + suffixWordC;
        // console.info(dc);
        result += dc.replace(regExp,'').length;

        for (let suffixWordA of a) {
          let dca = prefixWord + ' and ' + suffixWordC + '-' + suffixWordA;
          // console.info(dca);
          result += dca.replace(regExp,'').length;
        }
      }
      result += prefixWord.replace(regExp,'').length;
    }

    // Lastly, 1000.
    for (let word of e) { result += word.replace(regExp, '').length; }

    document.getElementById('result').innerHTML = result;
}